/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
* Fichero: util.java
* @author Jónatan Recuenco <jonatanrecuenco@hotmail.com>
* @date 15-nov-2013
*/

public class util {
    
    public static boolean validar (String s){
        Pattern p = Pattern.compile("[\\w]+@[\\w]+\\.\\w+");
        Matcher m = p.matcher(s);
        return m.matches();
    }
    
    public static void fin(){
        System.out.println("FIN");
    }
}